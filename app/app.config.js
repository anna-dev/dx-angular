(function(app){
    app.config(['$locationProvider', '$stateProvider', '$urlRouterProvider',  function($locationProvider, $stateProvider, $urlRouterProvider) {

       // $locationProvider.html5Mode(true);

        $urlRouterProvider
            .when('/settings', '/settings/settings-general')
            .when('/settings/settings-api', '/settings/settings-api/settings-api-list')
            .otherwise("/");

        //
        // Now set up the states
        $stateProvider
            .state('home', {
                url: "/",
                templateUrl: "app/pages/home/home.html",
                controller: 'HomeCtrl'
            })
            .state('dashboard', {
                url: "/dashboard",
                templateUrl: "app/pages/dashboard/dashboard.html",
                controller: 'DashboardCtrl'
            })
            .state('clients', {
                url: "/clients",
                templateUrl: "app/pages/clients/clients.html",
                controller: 'ClientsCtrl'
            })

            .state('reports', {
                url: "/reports",
                templateUrl: "app/pages/404/404.html",
                controller: '404Ctrl'
            })
            .state('general-styles', {
                url: "/general-styles",
                templateUrl: "app/pages/general-styles/general-styles.html",
                controller: 'GeneralStylesCtrl'
            })

            .state('settings', {
                url: "/settings",
                templateUrl: "app/pages/settings/settings.html",
                controller: 'SettingsCtrl'
            })

            .state( "settings.settings-api",  {
                url: '/settings-api',
                controller: 'SettingsApiCtrl',
                templateUrl: 'app/pages/settings-api/settings-api.html'
            })

                .state( "settings.settings-api.settings-api-list",  {
                    url: '/settings-api-list',
                    controller: 'SettingsApiListCtrl',
                    templateUrl: 'app/pages/settings-api-list/settings-api-list.html'
                })
                .state( "settings.settings-api.settings-api-detail",  {
                    url: '/settings-api-detail',
                    controller: 'SettingsApiDetailCtrl',
                    templateUrl: 'app/pages/settings-api-detail/settings-api-detail.html'
                })
            .state( "settings.settings-general",  {
                url: '/settings-general',
                controller: 'SettingsGeneralCtrl',
                templateUrl: 'app/pages/settings-general/settings-general.html'
            })
            .state( "settings.settings-survey",  {
                url: '/settings-survey',
                controller: 'SettingsSurveyCtrl',
                templateUrl: 'app/pages/settings-survey/settings-survey.html'
            });


    }]);
}(angular.module('app')));