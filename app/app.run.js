(function(app){
    app.run(['$rootScope', '$location', function ($rootScope, $location) {
        var path = function() { return $location.path();};
        $rootScope.$watch(path, function(newVal, oldVal){
            $rootScope.activetab = newVal;
        });
    }]);
}(angular.module('app')));