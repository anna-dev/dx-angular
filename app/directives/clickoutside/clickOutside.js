(function(app){
    app.factory("$mdDatePicker", ["$mdDialog", function($mdDialog) {
        var datePicker = function(targetEvent, currentDate) {
            if (!angular.isDate(currentDate)) currentDate = Date.now();

            return $mdDialog.show({
                controller: 'DatePickerCtrl',
                controllerAs: 'datepicker',
                templateUrl: "/modal.datepicker.html",
                targetEvent: targetEvent,
                locals: {
                    currentDate: currentDate
                }
            });
        };

        return datePicker;
    }]);
}(angular.module('app')));
