/**
 * Created by master on 27.02.2017.
 */
(function(app){
    app.directive("input", ["$mdDatePicker", "$timeout", function($mdDatePicker, $timeout) {
        return {
            restrict: 'E',
            require: '?ngModel',
            link: function(scope, element, attrs, ngModel) {
                if ('undefined' !== typeof attrs.type && 'date' === attrs.type && ngModel) {
                    angular.element(element).on("click", function(ev) {
                        $mdDatePicker(ev, ngModel.$modelValue).then(function(selectedDate) {
                            $timeout(function() {
                                ngModel.$setViewValue(moment(selectedDate).format("YYYY-MM-DD"));
                                ngModel.$render();
                            });
                        });
                    });
                }
            }
        }
    }]);

}(angular.module('app')));