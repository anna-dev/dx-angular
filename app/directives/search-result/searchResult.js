(function(app){
    app.directive("searchResult", function(){
        return {
            restrict: 'AECM', // E- element , A-attribute; AE - is default ,   C - for class , M - comment
            templateUrl: 'app/directives/search-result/search-result.html',
            replace: true,
            controller: ['$scope', '$rootScope', function($scope, $rootScope) {
                    $scope.name = 'john Doe';
                    $scope.address = '111 Main St., New Yourk, NY 55555';

                console.log(app.controller);
            }],
            scope: {
                name: "@personName",
                address: "@personAddress"
            }
        }
    });
}(angular.module('app')));