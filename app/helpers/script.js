(function ($) {

    function gridFunc(){

        $(document).bind('keydown',function(e){
            var gridon = "on";
            if(e.keyCode == 88) {
                if ($('.dev-grid').hasClass(gridon)) {
                    $('.dev-grid').removeClass(gridon);
                }
                else {
                    $('.dev-grid').addClass(gridon);
                }
            }
        });

    }

    //---on document ready, all functions & conditions will be triggered below---------//

    $(function () {

        gridFunc();

        var mySVGsToInject = document.querySelectorAll('.svg');
        SVGInjector(mySVGsToInject);


        (function() {
            var isIE11 = !!(navigator.userAgent.match(/Trident/) && !navigator.userAgent.match(/MSIE/));
            if (isIE11) {
                if (typeof window.attachEvent == "undefined" || !window.attachEvent) {
                    window.attachEvent = window.addEventListener;
                }
            }

            var ieVerN, className;

            function GetIEVersion() {
                var sAgent = window.navigator.userAgent;
                var Idx = sAgent.indexOf("MSIE");
                // If IE, return version number.
                if (Idx > 0)
                    return parseInt(sAgent.substring(Idx + 5, sAgent.indexOf(".", Idx)));
                // If IE 11 then look for Updated user agent string.
                else if (!!navigator.userAgent.match(/Trident\/7\./))
                    return 11;
                else
                    return 0;
                //It is not IE
            }

            ieVerN = GetIEVersion();

            if (ieVerN > 0) {

                className = 'ie';

                if (ieVerN === 11) {
                    className += ' ie11';
                }else if (ieVerN === 10) {
                    className += ' ie10';
                }else if (ieVerN === 9) {
                    className += ' ie9';
                }else if (ieVerN <= 8) {
                    className += ' ie8';
                }

                $('body').addClass(className);

            }

        })();


        $(window).scroll(function() {


        });

        $(window).resize(function(){ });


    });

})(jQuery);
