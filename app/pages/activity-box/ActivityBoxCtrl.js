
(function(app) {
    app.controller('ActivityBoxCtrl', ['$scope',  function($scope){

        $scope.list = {
            "1": {
                name : 'today',
                content: [{
                    src: 'resources/images/icon-trello.svg',
                    type: 'Conference Call',
                    clientname: 'Client Name',
                    time: '3:30pm - Today',
                    actionclass: 'green'
                }, {
                    src: 'resources/images/icon-trello.svg',
                    type: 'Conference Call',
                    clientname: 'Client Name',
                    time: '3:30pm - Today',
                    actionclass: 'green'
                }, {
                    src: 'resources/images/icon-trello.svg',
                    type: 'Conference Call',
                    clientname: 'Client Name',
                    time: '3:30pm - Today',
                    actionclass: 'red'
                }, {
                    src: 'resources/images/icon-trello.svg',
                    type: 'Conference Call',
                    clientname: 'Client Name',
                    time: '3:30pm - Today',
                    actionclass: 'green'
                }]
            },
            "2": {
                name: 'yesterday',
                content: [{
                    src: 'resources/images/icon-trello.svg',
                    type: 'Conference Call',
                    clientname: 'Client Name',
                    time: '3:30pm - Today',
                    actionclass: 'yellow'
                }, {
                    src: 'resources/images/icon-trello.svg',
                    type: 'Conference Call',
                    clientname: 'Client Name',
                    time: '3:30pm - Today',
                    actionclass: 'green'
                }, {
                    src: 'resources/images/icon-trello.svg',
                    type: 'Conference Call',
                    clientname: 'Client Name',
                    time: '3:30pm - Today',
                    actionclass: 'green'
                }, {
                    src: 'resources/images/icon-trello.svg',
                    type: 'Conference Call',
                    clientname: 'Client Name',
                    time: '3:30pm - Today',
                    actionclass: 'green'
                }]
            }
        };




        setTimeout(function(){

            ( function activityBoxIntro(){

                var introanimation2 = new TimelineMax({delay:0.2});
                introanimation2.to("html", 0.3, {opacity: 1, ease:Cubic.easeInOut})
                    .from(".outer .activity-box", 0.5, {opacity:0, ease: Expo.easeInOut}, "same1")
                    .staggerFrom(".outer .activity-box .act-item, .outer .activity-box .act-date ", 0.7, {opacity:0, ease: Expo.easeInOut},0.1, "same1")
                ;

            })();

            var mySVGsToInject = document.querySelectorAll('.svg');
            SVGInjector(mySVGsToInject);
        }, 100);


    }]);
}(angular.module('app')));