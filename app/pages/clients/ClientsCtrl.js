(function(app) {
    app.controller('ClientsCtrl', ['$scope', '$filter',  function($scope, $filter){
        $scope.pageClass = 'clients';
        $scope.clients = [{
            class: 'arrow-down yellow',
            src: 'resources/images/c-logo-2.png',
            name: 'Starbucks-1',
            trending: '2' /*yellow is 2/green is 1/ red is 3  --> for trending filter*/
        }, {
            class: 'arrow-up green',
            src: 'resources/images/c-logo-1.png',
            name: 'Starbucks',
            trending: '1'
        }, {
            class: 'arrow-down red',
            src: 'resources/images/c-logo-3.png',
            name: 'Starbucks-2',
            trending: '3'
        },{
            class: 'arrow-down red',
            src: 'resources/images/c-logo-4.png',
            name: 'Starbucks-3',
            trending: '3'
        },{
            class: 'arrow-down yellow',
            src: 'resources/images/c-logo-2.png',
            name: 'Starbucks-4',
            trending: '2'
        }, {
            class: 'arrow-up green',
            src: 'resources/images/c-logo-1.png',
            name: 'Starbucks-5',
            trending: '1'
        }, {
            class: 'arrow-down red',
            src: 'resources/images/c-logo-3.png',
            name: 'Starbucks-6',
            trending: '3'
        },{
            class: 'arrow-down red',
            src: 'resources/images/c-logo-4.png',
            name: 'Starbucks-7',
            trending: '3'
        },{
            class: 'arrow-down yellow',
            src: 'resources/images/c-logo-2.png',
            name: 'Starbucks-8',
            trending: '2'
        }, {
            class: 'arrow-up green',
            src: 'resources/images/c-logo-1.png',
            name: 'Starbucks-9',
            trending: '1'
        }, {
            class: 'arrow-down red',
            src: 'resources/images/c-logo-3.png',
            name: 'Starbucks-0',
            trending: '3'
        },{
            class: 'arrow-down red',
            src: 'resources/images/c-logo-4.png',
            name: 'Starbucks-10',
            trending: '3'
        }];


        $scope.sortBy = function(propertyName) {
            $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
            $scope.propertyName = propertyName;
        };
    }]);
}(angular.module('app')));


