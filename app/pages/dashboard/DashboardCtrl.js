(function(app) {
    app.controller('DashboardCtrl', ['$scope',  '$http',  function($scope,  $http){
        $scope.pageClass = 'dashboard';
        $scope.overview = [{
            activeclass: 'green-set',
            number: 7.8,
            title: 'average DX'
        }, {
            activeclass: '',
            number: 7,
            title: 'average DX'
        }, {
            activeclass: '',
            number: 7,
            title: 'average DX'
        }, {
            activeclass: '',
            number: 7,
            title: 'average DX'
        }];
        $scope.satisfaction = [{
            class: 'red',
            src: 'resources/images/circle-red.png',
            value: '25%',
            fillClass: 'p25',
            valueTitle: 'unhappy'
        }, {
            class: 'yellow',
            src: 'resources/images/circle-yellow.png',
            value: '35%',
            fillClass: 'p35',
            valueTitle: 'satisfied'
        }, {
            class: 'green',
            src: 'resources/images/circle-green.png',
            value: '70%',
            fillClass: 'p70',
            valueTitle: 'estatic'
        }];
        $scope.clients = [{
            class: ' arrow-down yellow',
            src: 'resources/images/c-logo-2.png',
            name: 'Starbucks'
        }, {
            class: ' arrow-up green',
            src: 'resources/images/c-logo-1.png',
            name: 'Starbucks'
        }, {
            class: ' arrow-down red',
            src: 'resources/images/c-logo-3.png',
            name: 'Starbucks'
        },{
            class: ' arrow-down red',
            src: 'resources/images/c-logo-4.png',
            name: 'Starbucks'
        }];

        $scope.rangeSelect = [
            { text: 'year' },
            { text: 'month' },
            { text: 'day' }
        ];

        /*date range*/
        $scope.range = moment().range("2012-11-05", "2013-01-25")

        $scope.change = function(){
            $scope.range2 = moment().range("2012-11-15", "2012-12-25")
        }

        $scope.markers = [
            {
                day: moment(),
                marker: "tutoti"
            },
            {
                day: moment().add(2, 'day'),
                marker: "tutotite"
            }
        ];

        _callthis = function(day) {
            console.log("call this: ", day);
        };

        _filter = function (day) {
            w = day.weekday();
            return w < 6 && w > 0;
        };

        var template = "<div class=\"mighty-picker__wrapperxx\">\n  <button type=\"button\" class=\"mighty-picker__prev-month\" ng-click=\"moveMonth(-1)\"><<</button>\n  <div class=\"mighty-picker__month\"\n    bindonce ng-repeat=\"month in months track by $index\">\n    <div class=\"mighty-picker__month-name\" ng-bind=\"month.name\"></div>\n    <table class=\"mighty-picker-calendar\">\n      <tr class=\"mighty-picker-calendar__days\">\n        <th bindonce ng-repeat=\"day in month.weeks[1]\"\n          class=\"mighty-picker-calendar__weekday\"\n          bo-text=\"day.date.format('dd')\">\n        </th>\n      </tr>\n      <tr bindonce ng-repeat=\"week in month.weeks\">\n        <td\n            bo-class='{\n              \"mighty-picker-calendar__day\": day,\n              \"mighty-picker-calendar__day--selected\": day.selected,\n              \"mighty-picker-calendar__day--disabled\": day.disabled,\n              \"mighty-picker-calendar__day--marked\": day.marker\n            }'\n            ng-repeat=\"day in week track by $index\" ng-click=\"select(day)\">\n            <div style=\"font-style: italic;\" class=\"mighty-picker-calendar__day-wrapper\"\n              bo-text=\"day.date.date()\"></div>\n            <div class=\"mighty-picker-calendar__day-marker-wrapper\">\n              <div class=\"mighty-picker-calendar__day-marker\"\n                ng-if=\"day.marker\"\n                ng-bind-template=\"\">\n              </div>\n            </div>\n        </td>\n      </tr>\n    </table>\n  </div>\n  <button type=\"button\" class=\"mighty-picker__next-month\" ng-click=\"moveMonth(1)\">>></button>\n</div>";

        $scope.date = null;
        $scope.multi = [moment(), moment().add(2, 'day'), moment().add(5, 'day')];
        $scope.mark = moment();
        $scope.options = {months: 1, after: moment(), template: template};
        $scope.optionsM = {start: moment().add(-1, 'month'), months: 3, mode: "multiple", callback: _callthis, filter: _filter};
        $scope.optionsMAfter = moment().add(-1, 'month');
        $scope.optionsK = {months: 1};

        $scope.dateSt = moment();
        $scope.optionsSt = {};


        $scope.dateDbA = moment();
        $scope.optionsDbA = {};
        $scope.dateDbB =  moment().add(2, 'day');
        $scope.optionsDbB = {};

        $scope.addMarker = function() {
            $scope.markers.push({day: $scope.mark, marker: 'new marker'});
        };


/*range calendar*/
        $scope.selectedRange = {
            selectedTemplate: null,
            selectedTemplateName: 'DATE RANGE',
            dateStart: null,
            dateEnd: null,
            showTemplate: false,
            fullscreen: false
        };
    }]);

}(angular.module('app')));