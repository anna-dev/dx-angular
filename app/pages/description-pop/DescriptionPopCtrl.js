(function(app) {
    app.controller('DescriptionPopCtrl', ['$scope',  function($scope){
        /*select with img*/
        $scope.select2Config = {
            allowClear:true,
            tags: false,
            formatResult: $scope.format,
            formatSelection: $scope.format

        };
        $scope.rangeSelectValue = [
            { text: 'high', value: '1', img:'img' },
            { text: 'medium', value: '2', img:'img' },
            { text: 'low', value: '3', img:'img' }
        ];
        $scope.format = function (data) {

            var url = '';

            switch (data.text) {
                case 'high':
                    url = 'resources/images/high.png';
                    break;
                case 'medium':
                    url = 'resources/images/medium.png';
                    break;
                case 'low':
                    url = 'resources/images/low.png';
                    break;
            }

            return  "<img src='" + url +"' />&nbsp;&nbsp;" + data.text;
        };

        $scope.select2Config = {
            formatResult: $scope.format,
            formatSelection: $scope.format
        };

        $scope.closePopup2 = function(){
            var pop1_1 = new TimelineMax();

            pop1_1.to(".description-pop .container", 0.5, {opacity:0, top: -93, ease:Cubic.easeOut}, "same1")
                .to('.description-pop', 0.5, {opacity: 0, ease:Cubic.easeInOut}, "same1")
                .set('.description-pop', {className:"-=open"})
            ;
        };

    }]);
}(angular.module('app')));