(function(app) {
    app.controller('HeaderCtrl', ['$scope', '$http', '$timeout', '$filter',  function($scope, $http, $timeout, $filter){

        $scope.popupshowInteraction = function() {

            var pop1_2 = new TimelineMax();
            pop1_2.set('.interaction-pop', {className:"+=open"})
                .to(".interaction-pop .container", 0, {opacity:0, top: -93, ease:Cubic.easeOut})

                .to('.interaction-pop', 0.5, {opacity: 1, ease:Cubic.easeInOut}, "same1")
                .to(".interaction-pop .container", 0.5, {opacity:1, top: 0, ease:Cubic.easeOut}, "same1")
            ;

        };

        $scope.popupshowLogin = function() {
            var pop = new TimelineMax();
            pop.set('.login-pop', {className:"+=open"})
                .to(".login-pop .container", 0, {opacity:0, top: -93, ease:Cubic.easeOut})

                .to('.login-pop', 0.5, {opacity: 1, ease:Cubic.easeInOut}, "same1")
                .to(".login-pop .container", 0.5, {opacity:1, top: 0, ease:Cubic.easeOut}, "same1")
            ;

        };

        $scope.sideboard = function(){
            $('.t1').toggleClass('active');
            $('.mini-dash .feed').toggleClass('close');
        };

        ( function pageIntro(){

            var introanimation3 = new TimelineMax({delay:0.2});
            introanimation3.to("html", 0.3, {opacity: 1, ease:Cubic.easeInOut})
                .from(".header", 0.7, {top: -93, ease:Cubic.easeOut}, "same1")
                .staggerFrom(".header *", 0.7, {opacity:0, top: 90, ease: Back.easeOut.config(1.7)},0.2, "same1")
            ;

        })();
    }]);
}(angular.module('app')));