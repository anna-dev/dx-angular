(function(app) {
    app.controller('HomeCtrl', ['$scope',  '$http', '$mdDateRangePicker', '$mdDatePicker',  function($scope,  $http, $mdDateRangePicker, $mdDatePicker){
        $scope.pageClass = 'home';
        $scope.overview = [{
            activeclass: 'green-set',
            number: 7.8,
            title: 'average DX'
        }, {
            activeclass: '',
            number: 7,
            title: 'average DX'
        }, {
            activeclass: '',
            number: 7,
            title: 'average DX'
        }, {
            activeclass: '',
            number: 7,
            title: 'average DX'
        }];
        $scope.satisfaction = [{
            class: 'red',
            src: 'resources/images/circle-red.png',
            value: '25%',
            fillClass: 'p25',
            valueTitle: 'unhappy'
        }, {
            class: 'yellow',
            src: 'resources/images/circle-yellow.png',
            value: '35%',
            fillClass: 'p35',
            valueTitle: 'satisfied'
        }, {
            class: 'green',
            src: 'resources/images/circle-green.png',
            value: '70%',
            fillClass: 'p70',
            valueTitle: 'estatic'
        }];
        $scope.clients = [{
            class: ' arrow-down yellow',
            src: 'resources/images/c-logo-2.png',
            name: 'Starbucks'
        }, {
            class: ' arrow-up green',
            src: 'resources/images/c-logo-1.png',
            name: 'Starbucks'
        }, {
            class: ' arrow-down red',
            src: 'resources/images/c-logo-3.png',
            name: 'Starbucks'
        },{
            class: ' arrow-down red',
            src: 'resources/images/c-logo-4.png',
            name: 'Starbucks'
        }];
        /*select*/
        $scope.rangeSelect = [
            { text: 'year' },
            { text: 'month' },
            { text: 'day' }
        ];


        /*range calendar*/
        $scope.selectedRange = {
            selectedTemplate: null,
            selectedTemplateName: 'DATE RANGE',
            dateStart: null,
            dateEnd: null,
            showTemplate: false,
            fullscreen: false
        };



    }]);

}(angular.module('app')));