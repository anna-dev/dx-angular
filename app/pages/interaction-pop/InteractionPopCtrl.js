(function(app) {
    app.controller('InteractionPopCtrl', ['$scope','$mdDatePicker',  function($scope, $mdDatePicker){
/*datepicker*/
        this.startDate = function(ev) {
            $mdDatePicker(ev, $scope.startDate).then(function(selectedDate) {
                $scope.startDate = selectedDate;

                var now = selectedDate;
                var then = $scope.endDate;
                var ms = moment(now, "DD/MM/YYYY HH:mm:ss").diff(moment(then, "DD/MM/YYYY HH:mm:ss"));
                var d = moment.duration(ms);
                $scope.dateDifferenceDays = d.asDays();
            });
        }
        this.endDate = function(ev) {
            $mdDatePicker(ev, $scope.endDate).then(function(selectedDate) {
                $scope.endDate = selectedDate;

                var now = $scope.startDate;
                var then = selectedDate;
                var ms = moment(now, "DD/MM/YYYY HH:mm:ss").diff(moment(then, "DD/MM/YYYY HH:mm:ss"));
                var d = moment.duration(ms);
                $scope.dateDifferenceDays = d.asDays();
            });
        }
        /*tabs*/

        $scope.tabs = [{
            title: 'Interaction',
            url: 'one.tpl.html',
            ico: 'inter'
        }, {
            title: 'Events',
            url: 'two.tpl.html',
            ico: 'event'
        }, {
            title: 'Notes',
            url: 'three.tpl.html',
            ico: 'notes'
        }];

        $scope.currentTab = 'one.tpl.html';

        $scope.onClickTab = function (tab) {
            $scope.currentTab = tab.url;
        };

        $scope.isActiveTab = function(tabUrl) {
            return tabUrl == $scope.currentTab;
        };
        $scope.closePopup1 = function(){
            var pop1_1 = new TimelineMax();

            pop1_1.to(".interaction-pop .container", 0.5, {opacity:0, top: -93, ease:Cubic.easeOut}, "same1")
                .to('.interaction-pop', 0.5, {opacity: 0, ease:Cubic.easeInOut}, "same1")
                .set('.interaction-pop', {className:"-=open"})
            ;
        };
        /*select*/
        $scope.rangeSelectClient = [
            { text: 'client 1', value: '1'},
            { text: 'client 2', value: '2' },
            { text: 'client 3', value: '3' }
        ];
        $scope.rangeSelectScore = [
            { text: '1', value: '1' },
            { text: '2', value: '2' },
            { text: '3', value: '3' },
            { text: '4', value: '4' },
            { text: '5', value: '5' },
            { text: '6', value: '6' },
            { text: '7', value: '7' },
            { text: '8', value: '8' },
            { text: '9', value: '9' },
            { text: '10', value: '10' }
        ];
        /*tags*/
        $scope.tags = [
            { text: 'just' },
            { text: 'some' },
            { text: 'cool' },
            { text: 'tags' }
        ];
        $scope.loadTags = function(query) {
            return $http.get('/tags?query=' + query);
        };

        $scope.tags2 = [
            { "name": "Bush", "class": "type2" },
            { "name": "Obama", "class": "type3" },
            { "name": "Trump", "class": "type1" }
        ];

    }]);
}(angular.module('app')));