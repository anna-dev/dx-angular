(function(app) {
    app.controller('LoginPopCtrl', ['$scope',  function($scope){

        $scope.closePopup = function(){
            var pop1_1 = new TimelineMax();

            pop1_1.to(".login-pop .container", 0.5, {opacity:0, top: -93, ease:Cubic.easeOut}, "same1")
                .to('.login-pop', 0.5, {opacity: 0, ease:Cubic.easeInOut}, "same1")
                .set('.login-pop', {className:"-=open"})
            ;
        };

        /*checkbox*/
        /*checkbox*/

        $scope.selected = [1];
        $scope.toggle = function (item, list) {
            var idx = list.indexOf(item);
            if (idx > -1) {
                list.splice(idx, 1);
            }
            else {
                list.push(item);
            }
        };

        $scope.exists = function (item, list) {
            return list.indexOf(item) > -1;
        };

        $scope.isIndeterminate = function() {
            return ($scope.selected.length !== 0 &&
            $scope.selected.length !== $scope.items.length);
        };

        $scope.isChecked = function() {
            return $scope.selected.length === $scope.items.length;
        };

        $scope.toggleAll = function() {
            if ($scope.selected.length === $scope.items.length) {
                $scope.selected = [];
            } else if ($scope.selected.length === 0 || $scope.selected.length > 0) {
                $scope.selected = $scope.items.slice(0);
            }
        };


        /*list*/
        $scope.items = [{
            id: 'remember',
            label: 'Remember my password'
        }];
    }]);
}(angular.module('app')));