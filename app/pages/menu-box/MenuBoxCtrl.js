(function(app) {
    app.controller('MenuBoxCtrl', ['$scope',  function($scope){
        ( function pageIntro(){

            var introanimation = new TimelineMax({delay:0.2});
            introanimation.to("html", 0.3, {opacity: 1, ease:Cubic.easeInOut})
                .from(".outer .menu-box", 0.5, {opacity:0, ease: Expo.easeInOut}, "same1")
                .staggerFrom(".outer .menu-box .tab", 0.8, {right:"-100%", ease: Expo.easeInOut},0.1)
            ;

        })();

    }]);
}(angular.module('app')));
