(function(app) {
    app.controller('SettingsApiDetailCtrl', ['$scope',  function($scope){
        /*select*/

        $scope.rangeSelectValue = [
            { value: '1', text:'A-Z' },
            { value: '2', text:'Z-A' }
        ];

        /*checkbox*/

        $scope.selected = [1];
        $scope.toggle = function (item, list) {
            var idx = list.indexOf(item);
            if (idx > -1) {
                list.splice(idx, 1);
            }
            else {
                list.push(item);
            }
        };

        $scope.exists = function (item, list) {
            return list.indexOf(item) > -1;
        };

        $scope.isIndeterminate = function() {
            return ($scope.selected.length !== 0 &&
            $scope.selected.length !== $scope.items.length);
        };

        $scope.isChecked = function() {
            return $scope.selected.length === $scope.items.length;
        };

        $scope.toggleAll = function() {
            if ($scope.selected.length === $scope.items.length) {
                $scope.selected = [];
            } else if ($scope.selected.length === 0 || $scope.selected.length > 0) {
                $scope.selected = $scope.items.slice(0);
            }
        };


    /*list*/
        $scope.items = [{
            id: 'id1',
            label: 'Company Name'
        }, {
            id: 'id2',
            label: 'Company Name'
        },{
            id: 'id3',
            label: 'Company Name'
        },{
            id: 'id4',
            label: 'Company Name'
        },{
            id: 'id5',
            label: 'Company Name'
        },{
            id: 'id6',
            label: 'Company Name'
        },{
            id: 'id7',
            label: 'Company Name'
        },{
            id: 'id8',
            label: 'Company Name'
        },{
            id: 'id9',
            label: 'Company Name'
        }];

    }]);
}(angular.module('app')));


