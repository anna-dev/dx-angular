(function(app) {
    app.controller('SettingsGeneralCtrl', ['$scope',  function($scope){
        var parentScope = $scope.$parent;
        parentScope.child = $scope;

        $scope.pageClass = 'Overview';
        /*tags*/

        $scope.loadTags = function(query) {
            return $http.get('/tags?query=' + query);
        };


        /*tags*/
        $scope.tags1 = [
            { "name": 'Jane', "class": "type1" },
            { "name": 'Jackson', "class": "type1" },
            { "name": 'J Lo', "class": "type1" },
            { "name": 'Marshall Konev', "class": "type1" }
        ];
        $scope.tags2 = [
            { "name": "Jane Doe", "class": "type2" },
            { "name": "Freddy Crugger", "class": "type3" },
            { "name": "Teddy Bear", "class": "type1" },
            { "name": "Donald Trump", "class": "type1" }
        ];
        $scope.tags3 = [
            { "name": "Jane", "class": "type2" },
            { "name": "Freddy", "class": "type3" },
            { "name": "Obama", "class": "type1" },
            { "name": "Donald Duck", "class": "type1" }
        ];

        $scope.popDescription = function (){
            var popDescription = new TimelineMax();
            popDescription.set('.description-pop', {className:"+=open"})
                .to(".description-pop .container", 0, {opacity:0, top: -93, ease:Cubic.easeOut})

                .to('.description-pop', 0.5, {opacity: 1, ease:Cubic.easeInOut}, "same1")
                .to(".description-pop .container", 0.5, {opacity:1, top: 0, ease:Cubic.easeOut}, "same1")
            ;
        };

    }]);
}(angular.module('app')));


