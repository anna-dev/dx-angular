(function(app){
    app.service('firstService', function($http, $q) {

        var getItems = function () {
            var data = $q.defer();

            $http.get('/data.json').then(
                function(success) {
                    data.resolve(success.data);
                },
                function (error) {
                    data.reject(error);
                }
            );

            return data.promise;
        };

        return {
            getItems: getItems
        };
    });
}(angular.module('app')));


// firstService.getItems().then(function(items) { $scope.items = items; });

// $scope.items=???